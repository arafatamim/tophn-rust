use serde::Deserialize;
use std::thread;

#[derive(Deserialize)]
struct Story {
    title: String,
    url: String,
    score: i32,
    by: String,
}

fn main() {
    let stories_url = "https://hacker-news.firebaseio.com/v0/topstories.json";
    let item_url_base = "https://hacker-news.firebaseio.com/v0/item";

    let res = tinyget::get(stories_url).send().unwrap();
    let ids = serde_json::from_str::<Vec<i32>>(res.as_str().unwrap()).unwrap();
    let top_five = &ids[0..5];

    let mut threads = vec![];

    for id in top_five.to_owned() {
        threads.push(thread::spawn(move || {
            let res = tinyget::get(format!("{}/{}.json", item_url_base, id.to_string()).as_str())
                .send()
                .unwrap();
            let item = serde_json::from_str::<Story>(res.as_str().unwrap()).unwrap();

            println!(
                "{}\nScore: {}\nBy: {}\nURL: {}\n",
                item.title, item.score, item.by, item.url
            );
        }));
    }

    for thread in threads {
        thread.join().unwrap();
    }
}
